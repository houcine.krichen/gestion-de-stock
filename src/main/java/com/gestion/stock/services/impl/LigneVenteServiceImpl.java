package com.gestion.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.ILigneVenteDao;
import com.gestion.stock.entities.LigneVente;
import com.gestion.stock.services.ILigneVenteService;

@Transactional
public class LigneVenteServiceImpl implements ILigneVenteService {

	ILigneVenteDao dao;

	public void setDao(ILigneVenteDao dao) {
		this.dao = dao;
	}

	@Override
	public LigneVente save(LigneVente entity) {

		return dao.save(entity);
	}

	@Override
	public LigneVente update(LigneVente entity) {

		return dao.update(entity);
	}

	@Override
	public List<LigneVente> selectAll() {

		return dao.selectAll();
	}

	@Override
	public LigneVente getById(Long id) {

		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);

	}

	@Override
	public List<LigneVente> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public LigneVente findOne(String paramName, Object paramValue) {

		return dao.findOne(paramName, paramValue);
	}

	@Override
	public LigneVente findOne(String[] paramName, Object[] paramValue) {

		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {

		return dao.findCountBy(paramName, paramValue);
	}

}
