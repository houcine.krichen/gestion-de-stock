package com.gestion.stock.services.impl;

import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.gestion.stock.dao.IUtilisateurDao;
import com.gestion.stock.entities.Utilisateur;
import com.gestion.stock.services.IUtilisateurService;

@Transactional
public class UtilisateurServiceImpl implements IUtilisateurService {

	IUtilisateurDao dao;

	public void setDao(IUtilisateurDao dao) {
		this.dao = dao;
	}

	@Override
	public Utilisateur save(Utilisateur entity) {

		return dao.save(entity);
	}

	@Override
	public Utilisateur update(Utilisateur entity) {

		return dao.update(entity);
	}

	@Override
	public List<Utilisateur> selectAll() {

		return dao.selectAll();
	}

	@Override
	public Utilisateur getById(Long id) {

		return dao.getById(id);
	}

	@Override
	public void remove(Long id) {
		dao.remove(id);

	}

	@Override
	public List<Utilisateur> selectAll(String sortField, String sort) {
		// TODO Auto-generated method stub
		return dao.selectAll(sortField, sort);
	}

	@Override
	public Utilisateur findOne(String paramName, Object paramValue) {

		return dao.findOne(paramName, paramValue);
	}

	@Override
	public Utilisateur findOne(String[] paramName, Object[] paramValue) {

		return dao.findOne(paramName, paramValue);
	}

	@Override
	public int findCountBy(String paramName, Object paramValue) {

		return dao.findCountBy(paramName, paramValue);
	}

}
