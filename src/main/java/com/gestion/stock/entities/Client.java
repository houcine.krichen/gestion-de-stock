package com.gestion.stock.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@SuppressWarnings("serial")
@Entity
public class Client implements Serializable {

	@Id @GeneratedValue
	private Long idCLient ;
	private String nom ;
	private String prenom ;
	private String address ;
	private String photo ;
	private String email ;
	
	@OneToMany(mappedBy="client")
	private List<CommandeClient> commandeClients ;
	
	
	public Client() {
		// TODO Auto-generated constructor stub
	}
	
	public Long getIdCLient() {
		return idCLient;
	}
	public void setIdCLient(Long idCLient) {
		this.idCLient = idCLient;
	}
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoto() {
		return photo;
	}
	public void setPhoto(String photo) {
		this.photo = photo;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	
}
