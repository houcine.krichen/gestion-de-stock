package com.gestion.stock.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value="/home")
public class HomeController {
	
	@RequestMapping(value="/blank")
	public String index() {
		
		return "blank/blank";
	}

}
