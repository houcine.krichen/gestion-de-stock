<%@ include file="/WEB-INF/views/includes/include.jsp" %>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>blank - page </title>
    <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/dist/vendor/simple-line-icons/css/simple-line-icons.css">
    <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/dist/vendor/font-awesome/css/fontawesome-all.min.css">
    <link rel="stylesheet" href="<%=request.getContextPath() %>/resources/dist/css/styles.css">
</head>
<body class="sidebar-fixed header-fixed">
<div class="page-wrapper">
 
 
 <%@ include file="/WEB-INF/views/menu-top/menu-top.jsp" %>
 	

    <div class="main-container">
      
     <%@ include file="/WEB-INF/views/menu-left/side-bar.jsp" %>

        <div class="content">
			<h1>Blank page</h1>
        </div>
    </div>
</div>
<script src="<%=request.getContextPath() %>/resources/dist/vendor/jquery/jquery.min.js"></script>
<script src="<%=request.getContextPath() %>/resources/dist/vendor/popper.js/popper.min.js"></script>
<script src="<%=request.getContextPath() %>/resources/dist/vendor/bootstrap/js/bootstrap.min.js"></script>
<script src="<%=request.getContextPath() %>/resources/dist/vendor/chart.js/chart.min.js"></script>
<script src="<%=request.getContextPath() %>/resources/dist/js/carbon.js"></script>
<script src="<%=request.getContextPath() %>/resources/dist/js/demo.js"></script>
</body>
</html>
